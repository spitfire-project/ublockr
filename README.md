This package is licensed under GPL 3.0 (read [LICENSE](https://gitlab.com/spitfire-project/router-adblock/blob/master/LICENSE "LICENSE") for GPL Terms)

# What's this project about?

Tired of ads poping up or blinking annoyingly or even hijacking your browser, bloated adblockers for browers that hogs memory and only works on pc, want to block ads on your smartphone or tablet, got a router that supports entware ? Then my project might be something your interested in. Using your router to block annoying ads on every device connected to your wifi is now available with just a couple of commands, unleash the awesome power of entware today!

## Here is a short list of what it supports so far

* Blocks around 85 to 100k hosts
* IPv6 Support
* uses [Pixelserv-TLS](https://github.com/kvic-z/pixelserv-tls)
* Whitelist filter
* Automated System Detection ([Padavan](https://bitbucket.org/padavan/rt-n56u) / [OpenWrt](https://openwrt.org/) / [ASUSWRT-Merlin](https://github.com/RMerl/asuswrt-merlin/))
* Configuration file for settings.

### Planned features/support:
* [OpenWRT](https://openwrt.org/)
* Verify list integrity in case of defunct lists

# Prerequisite
1. A router supporting [entware](http://entware.net "entware")
2. A USB drive mounted on system with swap setup on it.
3. Changing port from 80 to another port number
4. Basic knowledge of linux

## Step 1 - Changing port of webinterface

### Padavan Firmware 

_Advanced Settings > Administration > Services_

Change port of web access from LAN to use port 8443

### AsusWRT-Merlin

_Administration > System > Web interface_

Enable only HTTPS and use port 8443

## Step 2 - Installing uBlockr

make sure to change config inside ublockr depending on if your running Padavan/AsusWRT-Merlin, default: AsusWRT-Merlin) Padavan (MIPSEL) 

then follow the installation instructions for installing entware at http://entware.net/ after entware is installed follow the rest of the instructions.

### AsusWRT Simple installation script
```
#!/bin/sh
wget https://gitlab.com/spitfire-project/ublockr/raw/master/ublockr -O /opt/bin/ublockr --no-check-certificate
wget https://gitlab.com/spitfire-project/ublockr/raw/master/ublockr.cfg -O /opt/etc/ublockr.cfg --no-check-certificate
chmod +x /opt/bin/ublockr
echo "cru a ublockr \"0 0 * * * /opt/bin/ublockr\"" >> /jffs/scripts/services-start
exit 0
```
### Padavan Installation script
```
#!/bin/sh
wget https://gitlab.com/spitfire-project/ublockr/raw/master/ublockr -O /opt/bin/ublockr --no-check-certificate
wget https://gitlab.com/spitfire-project/ublockr/raw/master/ublockr.cfg -O /opt/etc/ublockr.cfg --no-check-certificate
chmod +x /opt/bin/ublockr
opkg install cron
sed -i 's|root|admin|g' /opt/etc/crontab
echo "0 0 * * *  admin /opt/bin/ublockr" >> /opt/etc/crontab
exit 0
```

### Asuswrt-merlin
_To have uBlockr autoupdate you need to add it to services-start event this will make uBlockr run at 00:00 every night just change the value to whatever you want same as cron_


```
echo "cru a ublockr \"0 0 * * * /opt/bin/ublockr\"" >> /jffs/scripts/services-start
 ```

### Padavan Firmware
To have ublockr run on midnight every night consult this wiki page for Padavan firmware https://bitbucket.org/padavan/rt-n56u/wiki/EN/UsingCron
then simply add it on the crontab in the webui if you didnt use the above installation script.
```
0 0 * * * /opt/bin/ublockr
```
### Notes
To configure ublockr see the ublockr.cfg files for options AsusWRT not Merlin users have to specify others due to broken system detetection,
the parameter to what lan interface ublockr uses set nic parameter to use the lan interface to the interface used for lan.

* AsusWRT uses br0
* Padavan uses br0
* Openwrt uses br-lan

Rest of the configuration should be pretty obvious regarding paths etc.

If ublockr blocks a domain that your actively using add that domain to whitelist.filter in your ublockr folder path.

## Step 3 - I found a bug, what now?
if that happens then feel free to report any issues here https://gitlab.com/spitfire-project/ublockr/issues/

## Step 4 - The really optional stuff 

Feel free to improve upon this project and please backlink to this project
### Tested models
* Asus RT-N56U [Padavan Firmware](https://bitbucket.org/padavan/rt-n56u "Padavan Firmware")
* Asus RT-AC56U [Asuswrt-Merlin](https://github.com/RMerl/asuswrt-merlin "Asuswrt-Merlin Firmware")

[Click here to report working models and firmware](https://gitlab.com/spitfire-project/ublockr/issues/1)

## Screenshots
Here are some screenshot of how it works

<a href="https://gitlab.com/spitfire-project/ublockr/raw/master/Screenshots/screenshot_syslog.png"><img src="https://gitlab.com/spitfire-project/ublockr/raw/master/Screenshots/screenshot_syslog.png" height="62" width="585"></a>
<a href="https://gitlab.com/spitfire-project/ublockr/raw/master/Screenshots/screenshot_term.png"><img src="https://gitlab.com/spitfire-project/ublockr/raw/master/Screenshots/screenshot_term.png" height="90" width="585"></a>
